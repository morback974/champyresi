const con = require('./bdd/connection').con;
const timeInterval = require('./bdd/timeInterval');

function insert(dataArduino){
    let sql = "insert into log (dataArduino) values ('" + dataArduino + "');";
    // console.log(sql);

    con.query(sql, (err, result) => {
        if (err) console.log("Erreur enregistrement data");
        else{
            console.log("Enregistrement donnée effectué");
        }
    })
}

async function selectLast(){
    let sql = "select * from log order by id desc limit 1";

    return new Promise((resolve, reject) => {
        con.query(sql, (err, result) => {
            //if (err) throw err;
            if(result.length > 0)
                resolve(result[0].dataArduino);
            else{
                reject("Aucune derniere valeur");
            }
        })
    });
}

module.exports = {
    insert: insert,
    selectLast: selectLast,
    timeInterval: timeInterval
}