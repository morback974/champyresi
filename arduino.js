const {calMoy, delay} = require('./util/utilitaire');

let five = require('johnny-five');
let board = null;
let portUsb = "";
let client = null;

const arduino = {
    listPort : () => {
        return serialPort.list();
    },
    
    connectBoard : (port) => {        

        portUsb = port;
        return new Promise((resolve, reject) => {
            board = new five.Board(
                {   port:port,
                    repl:true}
                );
            board.on("ready", () => {
                initPin();

                console.log(this.repl);
                resolve();
            })
        })
    },

    program: require('./gestionChampignon'),
    
    launchProgramme(){
        program.lauch();
    },

    nbReco: 0,

    shutDown(){
        //board.repl.context.board[0].id = null
        console.log(board.repl.close());
    }
}

let listPin = [];

function initPin(){
    // Capteur Air
    listPin["A0"] = new five.Pin("A0");
    
    // Capteur Humide
    listPin["A1"] = new five.Pin("A1");

    // Capteur Sec
    listPin["A2"] = new five.Pin("A2");

    // Vanne Fermeture
    listPin[25] = new five.Pin(45);

    // Vanne Switch, combine avec la vanneAirF cela permet d'ouvrir
    listPin[27] = new five.Pin(47);

    // Ventilo Capteur
    listPin[7] = new five.Pin(7);

    // Brume
    listPin[31] = new five.Pin(39);

    // deshum
    listPin[6] = new five.Pin(6);

    // Met en etat fermer apr défault
    arduino.turnHigh([7, 25, 27, 31, 6]);

    arduino.emitEtatRelay();
}

arduino.getTemperature = (pin) =>{
    const NBMESURE = 20; // nb valeur relevé
    const NBVALMINI = 15; // nb valeur requis correcte pour calcul temperature
    const WAITTIME = 500;

    let listValeur = [];
    
    //console.log("Temps estimé relever de temperature => " +  (WAITTIME * NBMESURE / 1000) + " secondes");

    return new Promise(async (resolve, reject) => {
        console.log("Debut Temperature ARDUINO");

        for(let nbMesure = 0; nbMesure < NBMESURE; nbMesure++){
            let valAnalog = 0;

            try{
                valAnalog = await getValAnalogique(pin);
                //console.log("Valeur Analogique");
                //console.log(valAnalog);
            }catch(err){
                //console.log("Valeur Analogique Erreur");
                //console.log(err);

                console.log("Reconnexion arduino => ");
                console.log(arduino.nbReco);

                arduino.nbReco++;
                arduino.shutDown();

                //await arduino.connectBoard(portUsb);
                //arduino.program.sendMesureErreur();
                continue;
            }
            
            if(valAnalog >= 205 && valAnalog <= 1023){
                listValeur.push(valAnalog);
                //console.log("ListValeur Analogique");
               //console.log(listValeur);
            }

            // attente entre chaque mesure
            await delay(WAITTIME);
        }
        
        // test nb Val
        if(listValeur.length < NBVALMINI){
            reject("Beaucoup de mesure incorrecte : " + listValeur.length + " sur " + NBMESURE);
        }

        // tri tableau
        listValeur.sort((a, b) => a - b);
        
        //purge
        const BORDREDUIT = 5;
        let listValeurPurger = listValeur.slice(BORDREDUIT, listValeur.length - BORDREDUIT);
        const moyTab = calMoy(listValeurPurger);
        const moyTemp = five.Fn.map(moyTab, 205, 1023, 100, 400) / 10;

        if(moyTemp > 10 && moyTemp < 40){
            //console.log("Resolve : ");
            //console.log(moyTemp);

            await delay(1, "seconde");

            resolve(moyTemp);
        }else{
            reject("Valeur Temperature incorrecte : " + moyTemp);
        }
    });
    
}

// Recupere une valeur analogique a partir d'un pin
async function getValAnalogique(pin){
    let statePin = 0;
    
    await new Promise((resolve, reject) => {
        let limitMesure = null;
        try{
            limitMesure = setTimeout(() => {
                reject("Temps de Mesure trop longue : err CHAMP01");
            }, 30000);

            listPin[pin].query((state) => {
                clearTimeout(limitMesure);
                resolve(state);
            });
        }catch(err){
            console.error("Valeur Analogique Pin " + pin);
            reject(err);
        }
    }).then((state) => {
        statePin = state;
    });

    return statePin.value ? statePin.value : 0;
}

arduino.turnHigh = (pins) => {
    if(Array.isArray(pins)){
        pins.forEach(pin => {
            listPin[pin].high();
        });
    }else{
        listPin[pins].high();
    }
}

arduino.turnLow = (pins) => {
    if(Array.isArray(pins)){
        pins.forEach(pin => {
            listPin[pin].low();
        });
    }else{
        listPin[pins].low();
    }
}

arduino.setupClient = (pclient) => {
    client = pclient;
}

arduino.emitEtatRelay = () => {
    listPin.forEach((element, index) => {
        element.query((state) => {
            client.sendEtatRelay(state, index);
        });
    });
}

arduino.switchPin = (pin, etat = null) => {
    if(etat){
        if(etat == 1){
            arduino.turnHigh(pin);
        }else{
            arduino.turnLow(pin);
        }

        arduino.emitEtatRelay();
    }else{
        listPin[pin].query((state) => {
            if(state.state == 0){
                arduino.turnHigh(pin);
            }else{
                arduino.turnLow(pin);
            }
    
            arduino.emitEtatRelay();
        });
    }

}

module.exports = arduino;