const express = require('express');
const app = express();
const router = express.Router();
require('./src/router/default')(app, router, express);

const server = app.listen(3000,() => {
    console.log('Serveur en cour 3000!');
});

let io = require('socket.io')(server, {cookie:false});
module.exports = io;

let client = require('./client');

io.on('connection', (socket) => {
  client.setupSocket(socket);
  client.sendEtat();
});

app.use('/', router);