const path = require('path');
const cons = require('consolidate');

module.exports = function(app, router, express){
    // view engine setup
    app.engine('html', cons.swig)
    app.set('public', path.join(__dirname, '../../../public'));
    app.set('view engine', 'html');
    app.use(
        express.static(__dirname + '../../../public')
    );

    //Home
    router.get('/', (req, res) => {
        res.type('html');
        let renderedViews = "";
        // res.sendFile(path.join(__dirname + '/public', 'paramArduino.html'));
      
        app.render(path.join(__dirname + '../../../public', 'header'), 
          (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'home'), 
            (err, html) => {
              renderedViews += html;
              res.send(renderedViews);
            }
          );
          }
        );
      });

    router.get('/paramArduino', (req, res) =>
        {
        res.type('html');
        let renderedViews = "";
        // res.sendFile(path.join(__dirname + '/public', 'paramArduino.html'));
        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'paramArduino'), 
                (err, html) => {
                renderedViews += html;
                res.send(renderedViews);
                }
            );
        }
        );
        });

    router.get('/gestionRelay', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
                app.render(path.join(__dirname + '../../../public', 'gestionRelay'), 
                (err, html) => {
                renderedViews += html;
                res.send(renderedViews)
                }
            );
            }
        );
    });

    router.get('/status', (req, res) => {
      let example = {
        up_time: Math.floor(process.uptime())
      };
      
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(example));
    })
}