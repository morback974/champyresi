const mysql = require('mysql');
const { text } = require('express');

let con = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "champyresi"
});

con.connect(async (err) => {
    if(err) throw err;
    console.log("Connected database");

    await new Promise((resolve, reject) => {
        let sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'champyresi' AND table_name = 'log';"
        con.query(sql, async (err, result) => {
            if(result.length == 0){
                await createTableLog();
            }else{
                //console.log("Log ok");
            }
            resolve();
        })
    }).then(async () => {
        await new Promise((resolve, reject) => {
            let sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'champyresi' AND table_name = 'timeinterval';"
            con.query(sql, async (err, result) => {
                if(result.length == 0){
                    await createTabletimeinterval();
                }else{
                    //console.log("timeinterval ok");
                }
                resolve();
            })
        })
    });
});

async function createTableLog(){
    await new Promise((resolve, reject) => {
        let sql = "create table log (id int not null auto_increment primary key, dataArduino text);";
        con.query(sql, (err, result) => {
            resolve();
        });
    })
}

async function createTabletimeinterval(){
    await new Promise((resolve, reject) => {
        let sql = "create table timeinterval (id int not null auto_increment primary key, libelle text, lastactivation datetime);";
        con.query(sql, (err, result) => {
            resolve();
        });
    })
}

module.exports = {con: con};