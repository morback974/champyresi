let io = require('./serveur');
let gestionChampignon = require('./gestionChampignon');
let serialPort = require('serialport');
let arduinoBoard = require('./arduino');

let socketIN = null;

function emitChoixPort(port){
    try{
        if(!gestionChampignon.isLaunch()){
          gestionChampignon.launch(port);
        }
      }catch(err){
        console.log(err);
      }
}

function setupSocket(socket){
    if (!socketIN) socketIN = socket;

    socket.on('choixPort', async (port) => {
        try{
          if(!gestionChampignon.isLaunch()){
            gestionChampignon.launch(port);
          }
        }catch(err){
          console.log(err);
        }
    });

    socket.on('reqListPort', () => {
        serialPort.list()
            .then((ports, err) => {
                if(!err){
                    io.emit('listPortName', ports.map(value => value.comName));
                }else{
                    console.log(err)
                }
        });
    });

    socket.on('saveJour', (nbJour) => {
        gestionChampignon.setJour(nbJour);
        emitDataArduino();
    });

    socket.on('dureeActivation', (dureeActivation) => {
        gestionChampignon.setDureeActivationBrume(dureeActivation);
        emitDataArduino();
    });

    socket.on("newConsigneAir", (consigneAir) => {
        gestionChampignon.setConsigneAir(consigneAir);
        emitDataArduino();
    });

    socket.on("newConsigneHum", (consigneHum) => {
        gestionChampignon.setConsigneHum(consigneHum);
        emitDataArduino();
    });

    socket.on("newModifAir", (modifAir) => {
        gestionChampignon.setModifConsigneAir(modifAir);
        emitDataArduino();
    });

    socket.on("newModifHum", (modifHum) => {
        gestionChampignon.setModifConsigneHum(modifHum);
        emitDataArduino();
    });

    socket.on("newEtalonageAir", (etalAir) => {
        gestionChampignon.setEtalonageAir(etalAir);
        emitDataArduino();
    });

    socket.on("newEtalonageSec", (etalSec) => {
        gestionChampignon.setEtalonageSec(etalSec);
        emitDataArduino();
    });

    socket.on("newEtalonageHum", (etalHum) => {
        gestionChampignon.setEtalonageHum(etalHum);
        emitDataArduino();
    });

    socket.on("etatRelay", () => {
        arduinoBoard.emitEtatRelay();
    });

    socket.on("switchRelayBrume", (infoSwitch) => {
        let gestionChampignon = require('./gestionChampignon');
        arduinoBoard.switchPin(infoSwitch.pin, infoSwitch.etatRelay);

        if(infoSwitch.etatRelay == 0){
            io.emit("delaySwitchRelayBrume", gestionChampignon.dataArduino.dureeActivationBrume);
        }

    });

    socket.on("initDataArduino", () => {
        let gestionChampignon = require('./gestionChampignon');
        gestionChampignon.initDataArduino();
    })
}

function sendEtat() {
    if(!gestionChampignon.isLaunch()){
        io.emit("connectPort", false);
    }else{
        io.emit("connectPort", true);
        emitDataArduino();
    }
}

function sendEtatRelay(state, index){
    io.emit("etatRelay", {state:state, index:index});
}

function emitDataArduino(val = null){
    if(val){
        io.emit('dataJson', val);
    }else{
        gestionChampignon.setEtape("", "", false);
    }
}

function messageMesureErreur(){
    io.emit('erreurMesure', true);
}


let client = {
    io:io,
    gestionChampignon:gestionChampignon,
    setupSocket:setupSocket,
    sendEtat:sendEtat,
    emitDataArduino:emitDataArduino,
    sendEtatRelay:sendEtatRelay,
    messageMesureErreur:messageMesureErreur,
    emitChoixPort:emitChoixPort
}

gestionChampignon.setupClient(client);
arduinoBoard.setupClient(client);

module.exports = {...client};