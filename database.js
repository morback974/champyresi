let mongo = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017";
let database_name = "champyresi";
let collection_name = "LOG";

function call(cb)
{
    mongo.connect(url, cb);
}
function createCollection()
{
    call(function(err,db){
        if (err) throw err;
        var dbo = db.db(database_name);
        dbo.createCollection(collection_name,function(err,res){
            if (err) throw err;
            //console.log("Collection Created");
            db.close();
        });
    });
}
function insert(data_array)
{
    call(function(err,db){
        if (err) throw err;
        var dbo = db.db(database_name);
        dbo.collection(collection_name).insertOne(data_array,function(err,res){
            if (err) throw err;
            //console.log(res.insertedCount+"insert"); //test (a retirer quand validé)
            db.close(); 
        });
    });
}
function find()
{
    call(function(err,db){
        if (err) throw err;
        var dbo = db.db(database_name); //nom a changer
        dbo.collection(collection_name).find({}).toArray(function(err,res){
            if (err) throw err;
            //console.log(res);
            db.close();
        });
    });
}
function findOne(data)
{
    call(function(err,db){
        if (err) throw err;
        var dbo = db.db(database_name); //nom a changer
        dbo.collection(collection_name).find({},{projection : data}).toArray(function(err,res){
            if (err) throw err;
            //console.log(res);
            db.close();
        });
    });
}
function update(data,new_data)
{
    call(function(err,db){
        if (err) throw err;
        var dbo = db.db(database_name); //nom a changer
        dbo.collection(collection_name).updateOne(data,{$set : new_data},function(err,res)
        {
            if (err) throw err;
            //console.log(res.result.nModified);
            db.close();
        });
    });
}
function remove(data)
{
    call(function(err,db){
        if (err) throw err;
        var dbo = db.db(database_name); //nom a changer
        dbo.collection(collection_name).deleteOne(data,function(err,res){
            if (err) throw err;
            //console.log(res.result.n)
            db.close();
        });
    });
}

async function selectLast(){

    let valEnregistrement = null;
    
    await new Promise((resolve, reject) => {
        call(function(err, db){
            if(err) throw err;
            var dbo = db.db(database_name); //nom a changer
            dbo.collection(collection_name).find().sort({'_id': -1}).limit(1).toArray(function(err, res){
                //console.log(res[0]);
                resolve(res[0]);
            });
        });
    }).then((val) => {
        valEnregistrement = val;
    })
    
    return valEnregistrement;
}

module.exports = {
    createCollection : createCollection(),

    insert : insert,

    find : find,

    findOne : findOne,

    update : update,

    remove : remove,
    
    selectLast : selectLast
}